import React, { useState, useEffect } from 'react';
import Slider from './components/slider/Slider';
import cats from './js/cats';

function App() {

  return (
    <>
      <Slider items={cats}/>
    </>
  )
}

export default App;
