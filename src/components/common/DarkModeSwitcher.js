import React, { useState, useEffect } from 'react';

const DarkModeSwitcher = (props) => {
    const [day, setDay] = useState(true);
    const [buttonText, setButtonText] = useState('Night');
    
    useEffect(() => {
        if (day){
            setButtonText('Dark mode');
            document.body.style.backgroundColor = "#ffffff";
        } else {
            setButtonText('Light mode');
            document.body.style.backgroundColor = "#000000";
        }
        props.callback(!day);
    }, [day])

    return <span>
        <button className="btn btn-primary btn-sm" onClick={() => setDay(x => !x)}>
            {buttonText}
        </button>
    </span>
}

export default DarkModeSwitcher;