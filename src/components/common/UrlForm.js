import React, { useState } from 'react';

const UrlForm = ({title, callback}) => {
    
    const [form, setForm] = useState({name: "", email: "", url: ""});
    const [validation, setValidation] = useState();

    const onFormChangeHandler = (e) => {
        const name = e.target.name;
        setForm({...form, [name]: e.target.value});
    }

    const onFormSubmitHandler = () => {
        if (form.name === ""
            || form.email === ""
            || form.url === ""){
                setValidation(false);
        } else {
            setValidation(true);
            callback(form);
        }
    };
    
    return (
        <div>
            <h2 hidden={!title}>{title}</h2>
            <div className="form-group">
                <label for="name">Imię i nazwisko</label>
                <input type="text" value={form.name} onChange={onFormChangeHandler} class="form-control" name="name"/>
            </div>
            <div className="form-group">
                <label for="email">Adres email</label>
                <input type="text" value={form.email} onChange={onFormChangeHandler} class="form-control" name="email"/>
            </div>
            <div className="form-group">
                <label for="url">Url</label>
                <input type="text" value={form.url} onChange={onFormChangeHandler} class="form-control" name="url"/>
            </div>
            <div className="alert" hidden={validation===undefined || validation!==true}>Formularz ok - wysyłam :)</div>
            <div className="alert" hidden={validation===undefined || validation!==false}>Uzupełnij wszystkie pola!</div>
            <button className="btn btn-primary" onClick={onFormSubmitHandler}>Dodaj</button>
        </div>
    );
};

export default UrlForm;