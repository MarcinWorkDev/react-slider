import React from 'react';

const LikeBadge = ({like}) => {
    if (like){
        return <span className="badge badge-success">Like!</span>;
    } else {
        return <span/>;
    }
};

export default LikeBadge;