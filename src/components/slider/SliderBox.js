import React from 'react';
import SliderBoxItem from './SliderBoxItem'

const SliderBox = ({index, item, likeCallback}) => {
    return <div className="card-body" 
        onClick={() => likeCallback(index)}
        style={{
            width: "100%",
            height: "200px",
            margin: 0,
            padding: 0
        }}>
        <SliderBoxItem url={item.url} addedBy={item.addedBy}/>
    </div>
}

export default SliderBox;