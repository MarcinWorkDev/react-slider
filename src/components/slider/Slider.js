import React, { useState, useEffect } from 'react';
import DarkModeSwitcher from '../common/DarkModeSwitcher'
import SliderBoxHeader from './SliderBoxHeader'
import SliderBox from './SliderBox'
import SliderBoxFooter from './SliderBoxFooter'
import SliderBoxNavigation from './SliderBoxNavigation';
import UrlForm from '../common/UrlForm';

const Slider = ({items}) => {

    const [index, setIndex] = useState(0);
    const [auto, setAuto] = useState(false);
    const [duration, setDuration] = useState();
    const [darkMode, setDarkMode] = useState(false);
    const [likes, setLikes] = useState([]);

    let autoId = null;
    useEffect(() => {
        if (auto){
            autoId = setInterval(() => {
                setIndex(idx => idx >= items.length-1 ? 0 : ++idx);
            }, duration);
        } else {
            clearInterval(autoId);
        }
        
        return () => clearInterval(autoId);
    }, [auto, duration]);

    const changeDarkMode = (callback) => {
        setDarkMode(callback);
    }

    const setImage = (auto, move) => {
        setAuto(auto);
        
        if (auto){
            setDuration(move);
            return;
        }

        const newPos = (current) => {
            let pos = (current+move) % items.length;
            pos = pos < 0 ? items.length-1 : pos;
            return pos;
        }
        setIndex(idx => newPos(idx));
    }

    const likeItem = (index) => {
        let like = (lks, idx) => {
            if (lks.includes(idx)){
                lks.splice(lks.indexOf(idx), 1);
                return [...lks];
            } else {
                return [...lks, idx];
            }
        }
        setLikes(lks => like(lks, index))
    }

    const addCat = ({name, url}) => {
        items.push({url: url, addedBy: name});
    };

    useEffect(() => {
    }, [items])

    return (
        <>
        <DarkModeSwitcher callback={changeDarkMode}/><br/><br/>
        <div className="card" style={{width: "410px", margin: "0 auto"}}>
            <div className="card-body">
                <div className="card">   
                    <SliderBoxHeader darkMode={darkMode} like={likes.includes(index)} title={"Gallery"}/><br/>
                    <SliderBox likeCallback={likeItem} index={index} item={items[index]}/><br/>
                    <SliderBoxFooter darkMode={darkMode} current={index+1} total={items.length}/>
                </div><br/>
                <SliderBoxNavigation moveCallback={setImage}/><br/>
                <UrlForm title="Dodaj nowego kota!" callback={addCat}/>
            </div>
        </div>
        </>
    );
};

export default Slider;