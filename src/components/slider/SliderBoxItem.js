import React from 'react';

const SliderBoxItem = ({url, addedBy}) => {
    return <>
            <div 
                style={{
                    width: "100%",
                    height: "100%",
                    transition: "background 1.5s",
                    transitionTimingFunction: "ease-out",
                    backgroundImage: `url('${url}')`,
                    backgroundSize: "400px 200px"
                }}></div>
                <p hidden={!addedBy}>Dodany przez: {addedBy}</p>
            </>;
}

export default SliderBoxItem;