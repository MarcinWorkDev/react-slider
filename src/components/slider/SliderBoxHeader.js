import React from 'react';
import LikeBadge from '../common/LikeBadge'

const SliderBoxHeader = ({title, darkMode, like}) => {
    return <div className={`card-header ${darkMode && "text-white bg-secondary"}`}>
        <span>{title} <LikeBadge like={like}/></span>
    </div>
}

export default SliderBoxHeader;