import React from 'react';

const SliderBoxFooter = ({current, total, darkMode}) => {
    return <div className={`card-footer ${darkMode && "text-white bg-secondary"}`}>
        {current ?? 0} / {total ?? 0}
    </div>
}

export default SliderBoxFooter;