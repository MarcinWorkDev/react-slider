import React, { useState, useEffect } from 'react';

const SliderBoxNavigation = ({moveCallback}) => {
    const [auto, setAuto] = useState(true);
    const [duration, setDuration] = useState(3);

    useEffect(() => {
        moveCallback(auto, duration*1000);
    }, [auto, duration]);

    const move = (auto, move) => {
        if (auto){
            setAuto(auto => !auto);
            setDuration(move);
        } else {
            moveCallback(auto, move);
        }
    }
    
    return (
        <div className="input-group" style={{width: "100%"}}>
            <div class="input-group-prepend">
                <button 
                    className="btn btn-danger"
                    style={{width: "100px"}}
                    disabled={auto}
                    onClick={e => move(false, -1)}>
                        Prev
                </button>
                <button 
                    className="btn btn-success"
                    style={{width: "100px"}}
                    disabled={auto}
                    onClick={e => move(false, 1)}>
                        Next
                </button>
            </div>
            <input 
                className="form-control"
                type="number"
                style={{width: "100px"}}
                disabled={!auto}
                onChange={(e) => setDuration(e.target.value)}
                value={duration}>
            </input>
            <div class="input-group-append">
                <button 
                    className="btn btn-primary"
                    onClick={e => move(true, duration)}>
                        Auto
                </button>
            </div>
        </div>
    );
};

export default SliderBoxNavigation;